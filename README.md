# sigcd-proxy

NGINX proxy for for our sigcd app

## Usage

### Evironment Variables

- `LISTEN_PORT` - Port to listen on (default: `8000`)
- `APP_HOST` - Hostname of the app to forward request to (default:`app`)
- `APP_PORT` - Port of the app to forward requests to (default:`9000`)
